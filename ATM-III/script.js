$(document).ready(function() {
	var account = {
		'1234' : {'pin': '1234', 'balance': 500},
		'5678' : {'pin': '5678', 'balance': 1500},
		'0000' : {'pin': '0000', 'balance': 2500}
	};
	
	$('.form-menu').hide();
  $('#displaybalance').hide();
	
	$('.form-signin').submit(function() {
		var accountNo = $('#accountNo').val();
		var pin = $('#pin').val();
		if (accountNo != null && account[accountNo]['pin'] == pin) {
			$('.form-menu').show();
      $('#displaybalance').show();
			$('.form-signin').hide();
			showbalance(accountNo);
      $('#karn').hide();
      $('#find').hide();
      $('#deposit').click(function(){
				var num = parseInt($('#a1').val());
				if (isNaN(num)|| num <= 0){
					alert("จำนวนเงินไม่ถูกต้อง !");
				} else {
					deposit(accountNo, num);
				}
        
			
			});

			$('#withdraw').click(function(){
				var num = parseInt($('#a2').val());
				if (isNaN(num)|| num <= 0){
					alert("จำนวนเงินไม่ถูกต้อง !");
				} else {
					if (num <= account[accountNo]['balance']){
						withdraw(accountNo, num);
					}else{
						alert("จำนวนเงินในการถอนไม่เพียงพอ !");
					}
				}
			});

			

		} else {
			alert('เลขที่บัญชีหรือรหัสผ่านไม่ถูกต้อง');
		}
		return false;
	});

	$('#beam').click(function(){
    $('#karn').toggle();
    $('#find').hide();
  });
  $('#wanna').click(function(){
    $('#find').toggle();
    $('#karn').hide();
  });
	
	function showbalance(accountNo) {
		$("#user").html('เลขที่บัญชี : ' + accountNo);
		$("#balance").html(account[accountNo]['balance'].toString() + ' บาท');
	}

	function deposit(accountNo, amount) {
		account[accountNo]['balance'] += amount;
		showbalance(accountNo);
    $('#karn').hide();
    
		alert('ทำรายการฝากเงินเรียบร้อยแล้ว');
	}

	function withdraw(accountNo, amount) {
		account[accountNo]['balance'] -= amount;
		showbalance(accountNo);
    $('#find').hide();

		alert('ทำรายการถอนเงินเรียบร้อยแล้ว');
	}
});













